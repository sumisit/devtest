/****** Object:  Table [dbo].[Addresses]    Script Date: 1-12-2014 10:36:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Addresses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AddressTypeID] [int] NOT NULL,
	[Street] [nvarchar](255) NULL,
	[HouseNumber] [int] NULL,
	[HouseNumberExtension] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[City] [nvarchar](255) NULL,
	[CountryCode] [char](2) NULL,
	[PeopleID] [int] NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressType]    Script Date: 1-12-2014 10:36:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressType](
	[ID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AddressType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 1-12-2014 10:36:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Countries](
	[Code] [char](2) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[People]    Script Date: 1-12-2014 10:36:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[People](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](255) NULL,
	[MiddleName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Addresses] ON 

GO
INSERT [dbo].[Addresses] ([ID], [AddressTypeID], [Street], [HouseNumber], [HouseNumberExtension], [ZipCode], [City], [CountryCode], [PeopleID]) VALUES (1, 1, N'Donaulaan', 58, NULL, N'1448JC', N'Purmerend', N'NL', 1)
GO
INSERT [dbo].[Addresses] ([ID], [AddressTypeID], [Street], [HouseNumber], [HouseNumberExtension], [ZipCode], [City], [CountryCode], [PeopleID]) VALUES (2, 2, N'Postbus', 905, NULL, N'1180AX', N'Amstelveen', N'NL', 1)
GO
INSERT [dbo].[Addresses] ([ID], [AddressTypeID], [Street], [HouseNumber], [HouseNumberExtension], [ZipCode], [City], [CountryCode], [PeopleID]) VALUES (3, 1, N'Dam', 1, NULL, N'1012JC', N'Amsterdam', N'NL', 2)
GO
INSERT [dbo].[Addresses] ([ID], [AddressTypeID], [Street], [HouseNumber], [HouseNumberExtension], [ZipCode], [City], [CountryCode], [PeopleID]) VALUES (4, 1, N'Binnenhof', 19, N'B', N'2513AA', N'Den Haag', N'NL', 3)
GO
SET IDENTITY_INSERT [dbo].[Addresses] OFF
GO
INSERT [dbo].[AddressType] ([ID], [Name]) VALUES (2, N'Delivery')
GO
INSERT [dbo].[AddressType] ([ID], [Name]) VALUES (1, N'Home')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AD', N'Andorra')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AE', N'Verenigde Arabische Emiraten')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AF', N'Afghanistan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AG', N'Antigua en Barbuda')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AI', N'Anguilla')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AL', N'Albanië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AM', N'Armenië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AO', N'Angola')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AQ', N'Antarctica')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AR', N'Argentinië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AS', N'Amerikaans-Samoa')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AT', N'Oostenrijk')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AU', N'Australië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AW', N'Aruba')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AX', N'Åland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'AZ', N'Azerbeidzjan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BA', N'Bosnië en Herzegovina')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BB', N'Barbados')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BD', N'Bangladesh')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BE', N'België')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BF', N'Burkina Faso')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BG', N'Bulgarije')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BH', N'Bahrein')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BI', N'Burundi')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BJ', N'Benin')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BL', N'Saint-Barthélemy')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BM', N'Bermuda')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BN', N'Brunei')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BO', N'Bolivia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BQ', N'Bonaire, Sint Eustatius en Saba')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BR', N'Brazilië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BS', N'Bahama''s')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BT', N'Bhutan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BV', N'Bouveteiland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BW', N'Botswana')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BY', N'Wit-Rusland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'BZ', N'Belize')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CA', N'Canada')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CC', N'Cocoseilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CD', N'Congo-Kinshasa')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CF', N'Centraal-Afrikaanse Republiek')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CG', N'Congo-Brazzaville')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CH', N'Zwitserland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CI', N'Ivoorkust')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CK', N'Cookeilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CL', N'Chili')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CM', N'Kameroen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CN', N'China')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CO', N'Colombia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CR', N'Costa Rica')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CU', N'Cuba')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CV', N'Kaapverdië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CW', N'Curaçao')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CX', N'Christmaseiland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CY', N'Cyprus')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'CZ', N'Tsjechië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DE', N'Duitsland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DJ', N'Djibouti')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DK', N'Denemarken')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DM', N'Dominica')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DO', N'Dominicaanse Republiek')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'DZ', N'Algerije')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'EC', N'Ecuador')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'EE', N'Estland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'EG', N'Egypte')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'EH', N'Westelijke Sahara')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ER', N'Eritrea')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ES', N'Spanje')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ET', N'Ethiopië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FI', N'Finland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FJ', N'Fiji')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FK', N'Falklandeilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FM', N'Micronesia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FO', N'Faeröer')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'FR', N'Frankrijk')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GA', N'Gabon')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GB', N'Verenigd Koninkrijk')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GD', N'Grenada')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GE', N'Georgië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GF', N'Frans-Guyana')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GG', N'Guernsey')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GH', N'Ghana')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GI', N'Gibraltar')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GL', N'Groenland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GM', N'Gambia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GN', N'Guinee')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GP', N'Guadeloupe')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GQ', N'Equatoriaal-Guinea')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GR', N'Griekenland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GS', N'Zuid-Georgia en de Zuidelijke Sandwicheilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GT', N'Guatemala')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GU', N'Guam')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GW', N'Guinee-Bissau')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'GY', N'Guyana')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HK', N'Hongkong')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HM', N'Heard en McDonaldeilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HN', N'Honduras')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HR', N'Kroatië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HT', N'Haïti')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'HU', N'Hongarije')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ID', N'Indonesië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IE', N'Ierland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IL', N'Israël')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IM', N'Man')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IN', N'India')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IO', N'Brits Indische Oceaanterritorium')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IQ', N'Irak')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IR', N'Iran')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IS', N'IJsland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'IT', N'Italië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'JE', N'Jersey')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'JM', N'Jamaica')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'JO', N'Jordanië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'JP', N'Japan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KE', N'Kenia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KG', N'Kirgizië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KH', N'Cambodja')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KI', N'Kiribati')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KM', N'Comoren')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KN', N'Saint Kitts en Nevis')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KP', N'Noord-Korea')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KR', N'Zuid-Korea')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KW', N'Koeweit')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KY', N'Kaaimaneilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'KZ', N'Kazachstan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LA', N'Laos')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LB', N'Libanon')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LC', N'Saint Lucia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LI', N'Liechtenstein')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LK', N'Sri Lanka')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LR', N'Liberia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LS', N'Lesotho')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LT', N'Litouwen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LU', N'Luxemburg')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LV', N'Letland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'LY', N'Libië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MA', N'Marokko')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MC', N'Monaco')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MD', N'Moldavië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ME', N'Montenegro')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MF', N'Sint-Maarten')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MG', N'Madagaskar')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MH', N'Marshalleilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MK', N'Macedonië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ML', N'Mali')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MM', N'Myanmar')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MN', N'Mongolië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MO', N'Macau')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MP', N'Noordelijke Marianen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MQ', N'Martinique')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MR', N'Mauritanië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MS', N'Montserrat')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MT', N'Malta')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MU', N'Mauritius')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MV', N'Maldiven')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MW', N'Malawi')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MX', N'Mexico')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MY', N'Maleisië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'MZ', N'Mozambique')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NA', N'Namibië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NC', N'Nieuw-Caledonië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NE', N'Niger')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NF', N'Norfolk')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NG', N'Nigeria')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NI', N'Nicaragua')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NL', N'Nederland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NO', N'Noorwegen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NP', N'Nepal')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NR', N'Nauru')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NU', N'Niue')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'NZ', N'Nieuw-Zeeland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'OM', N'Oman')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PA', N'Panama')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PE', N'Peru')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PF', N'Frans-Polynesië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PG', N'Papoea-Nieuw-Guinea')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PH', N'Filipijnen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PK', N'Pakistan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PL', N'Polen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PM', N'Saint-Pierre en Miquelon')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PN', N'Pitcairneilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PR', N'Puerto Rico')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PS', N'Palestina')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PT', N'Portugal')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PW', N'Palau')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'PY', N'Paraguay')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'QA', N'Qatar')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'RE', N'Réunion')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'RO', N'Roemenië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'RS', N'Servië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'RU', N'Rusland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'RW', N'Rwanda')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SA', N'Saoedi-Arabië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SB', N'Salomonseilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SC', N'Seychellen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SD', N'Soedan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SE', N'Zweden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SG', N'Singapore')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SH', N'Sint-Helena, Ascension en Tristan da Cunha')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SI', N'Slovenië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SJ', N'Spitsbergen en Jan Mayen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SK', N'Slowakije')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SL', N'Sierra Leone')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SM', N'San Marino')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SN', N'Senegal')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SO', N'Somalië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SR', N'Suriname')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SS', N'Zuid-Soedan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ST', N'Sao Tomé en Principe')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SV', N'El Salvador')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SX', N'Sint Maarten')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SY', N'Syrië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'SZ', N'Swaziland')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TC', N'Turks- en Caicoseilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TD', N'Tsjaad')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TF', N'Franse Zuidelijke en Antarctische Gebieden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TG', N'Togo')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TH', N'Thailand')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TJ', N'Tadzjikistan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TK', N'Tokelau')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TL', N'Oost-Timor')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TM', N'Turkmenistan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TN', N'Tunesië')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TO', N'Tonga')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TR', N'Turkije')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TT', N'Trinidad en Tobago')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TV', N'Tuvalu')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TW', N'Taiwan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'TZ', N'Tanzania')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'UA', N'Oekraïne')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'UG', N'Oeganda')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'UM', N'Kleine Pacifische eilanden van de Verenigde Staten')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'US', N'Verenigde Staten')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'UY', N'Uruguay')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'UZ', N'Oezbekistan')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VA', N'Vaticaanstad')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VC', N'Saint Vincent en de Grenadines')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VE', N'Venezuela')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VG', N'Britse Maagdeneilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VI', N'Amerikaanse Maagdeneilanden')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VN', N'Vietnam')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'VU', N'Vanuatu')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'WF', N'Wallis en Futuna')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'WS', N'Samoa')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'YE', N'Jemen')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'YT', N'Mayotte')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ZA', N'Zuid-Afrika')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ZM', N'Zambia')
GO
INSERT [dbo].[Countries] ([Code], [Name]) VALUES (N'ZW', N'Zimbabwe')
GO
SET IDENTITY_INSERT [dbo].[People] ON 

GO
INSERT [dbo].[People] ([ID], [FirstName], [MiddleName], [LastName]) VALUES (1, N'Jeroen', NULL, N'Kok')
GO
INSERT [dbo].[People] ([ID], [FirstName], [MiddleName], [LastName]) VALUES (2, N'Gabri', N'van', N'Lingen')
GO
INSERT [dbo].[People] ([ID], [FirstName], [MiddleName], [LastName]) VALUES (3, N'Theo-Paul', N'van', N'Kampen')
GO
SET IDENTITY_INSERT [dbo].[People] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UK_AddressType_Name]    Script Date: 1-12-2014 10:36:01 ******/
ALTER TABLE [dbo].[AddressType] ADD  CONSTRAINT [UK_AddressType_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Address_AddressType] FOREIGN KEY([AddressTypeID])
REFERENCES [dbo].[AddressType] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Address_AddressType]
GO
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Address_Country] FOREIGN KEY([CountryCode])
REFERENCES [dbo].[Countries] ([Code])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Address_Country]
GO
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_People] FOREIGN KEY([PeopleID])
REFERENCES [dbo].[People] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_People]
GO
